public class PostoCombustivel
{
    private double precoLitro;
    private double valorPago;

    public PostoCombustivel()
    {
     
    }
    
    public void setPrecoLitro(double paramPreco) {
        this.precoLitro = paramPreco;
    }
    
    public double getPrecoLitro() {
        return this.precoLitro;
    }
    
    public void setValorPago(double paramValorPago) {
        this.valorPago = paramValorPago;
    }
    
    public double getValorPago() {
        return this.valorPago;
    }
    
    public double calculaLitros() {
        double totalLitros = this.valorPago / this.precoLitro;
        return totalLitros;
    }
}
