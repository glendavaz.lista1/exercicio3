import java.util.Scanner;
public class Principal
{
    public static void main (String[] args) {
        Scanner le = new Scanner(System.in);  
        PostoCombustivel posto = new PostoCombustivel();
        
        System.out.println("Insira o preço do Litro da Gasolina:");
        posto.setPrecoLitro(le.nextDouble());
        
        System.out.println("Insira o valor pago:");
        posto.setValorPago(le.nextDouble());
        
        System.out.println("O total de litros colocados foi: "+ posto.calculaLitros());
    }
  
}
